import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificacionProvider {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  void initNotifications() {
    _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    _firebaseMessaging.setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );
  }

  Future<String> obtainToken() async {
    String token = await this._firebaseMessaging.getToken();
    return token;
  }
}
